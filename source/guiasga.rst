﻿====================
GUÍA RÁPIDA SGA
====================
.. Estas son las secciones que estaban en la descripción de la tarea del Jira
.. Guía de inicio (Registro de licencia, dispositivo, usuarios móvil)
.. Carga de información de los catálogos
.. Como generar Pase
.. Autorizar Pase
.. Crear Perfiles (Documentos, trabajadores)

Registro de Usuarios, Dispositivos, Licencias
=============================================

+ Para registrar un Usuario web, se podrá hacer en la sección Configuración/:ref:`sga-usuario-web`
	- En registro de usuarios, capturar los campos "Nombre y Apellidos, correo electrónico y contraseña"
	- Seleccionar el Rol del usuario: "Administrador del sistema ó Contratista"
	- En caso de ser Proveedor o Contratista, se debe seleccionar la empresa a la que pertenece
	- "GUARDAR"
	
.. hint:: Puede hacer búsqueda de usuarios, filtrando por rol y empresa


+ Registro de Usuario Móvil desde la sección Configuración/:ref:`sga-usuario-movil`
	- En registro de usuarios, capturar los campos "Nombre y Apellidos, Usuario móvil y contraseña"
	- Seleccionar el Rol del usuario: "Caseta vehicular, Caseta Peatonal"
	- "GUARDAR"


+ Para registrar un dispositivo se deberá llenar los siguientes datos en la sección Configuración/:ref:`dispositivo-movil`:
	- En dispositivos móviles se captura el modelo del dispositivo
	- Y el Identificador (Se encuentra en la pantalla de inicio en la aplicación móvil)
	- "GUARDAR"




Catálogos
==========
Para poder usar SGA es necesario cargar la información de las empresas, trabajadores y sus perfiles de documentos, herramientas y vehículos que ingresarán a sus instalaciones. Para ello existen los catálogos de estos campos para gestionar esta información.

+ Perfíl de Documentos/:ref:`sga-documentos`:
	- En esta sección se especifica la lista de documentos requeridos por tipo de trabajador, 
+ Registro de Empresa/:ref:`sga-empresa`:
	- En este catalogo se capturan las empresas que solicitaran pases para acceso
+ Registro de Trabajadores/:ref:`sga-trabajadores`:
	- En este apartado las empresas contratistas capturan los trabajadores que solicitarán pase para acceder a las instalaciones
+ Registro de Herramientas/:ref:`sga-herramienta`:
	- En este catalogo se capturan las herramientas que se adjuntaran a las solicitudes de pase
+ Registro de Vehículos/:ref:`sga-vehiculo`:
	- Aquí las empresas ingresarán los vehículos que se agregaran a las solicitudes de pase

.. hint:: En todos los catálogos mencionados anteriormente existe la funcionalidad de carga de la información por medio de plantillas csv que puede descargar de la misma plataforma en las secciones correspondientes.

Solicitudes de Pase
===================
+ Los pases se pueden generar o solicitar en la sección Pases/:ref:`sga-pase`, en el botón "Crear Pase", que pueden ser del tipo:
   - Normales
	+ Pases de acceso programado donde es necesario agregar, empleados, herramientas y/o vehículos 
   - Extraordinarios
	+ Son pases de emergencia para una visita rápida o no programada que solo puede generar un usuario administrador. En estos pases solo se captura el nombre de la persona, empresa, asunto y vigencia del pase. 

- Para creación de pases, le mostramos los pasos y una guia animada del procedimiento
	- Seleccionar tipo de pase
	- Seleccione empresa
	- Escriba el asunto por el cual se solicita el pase
	- En caso necesario seleccione herramientas y vehículo`
	- Seleccione área de visita
	- Indicar la vigencia del pase
	- "Generar Pase"

.. figure:: sga-web/sga-crear-pase.gif
   :height: 650 px
   :width: 1200 px
   :scale: 70 %
   :alt: alternate text
   :align: center
