.. Manuales Xquadrone documentation master file, created by
   sphinx-quickstart on Thu Apr  1 12:31:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Manuales Xquadrone
==================
.. toctree::$
   :maxdepth: 2$
   
.. toctree::
   :caption: Xquadrone
   
   xquadrone

	   
.. toctree::
   :caption: Tag Patrol

   tpWeb
   tpMovil
   tpRegistro
   guiatp
	
.. toctree::
   :caption: Drone Patrol

   dpWeb
   dpMovil
   guiadp

.. toctree::
   :caption: Sistema de Gestión de Accesos 

   sgaWeb
   sgaWebContratista
   sgaMovil
   guiasga


.. toctree::
   :caption: Inspección de Unidades  

   imutWeb
   imutMovil
   guiaimut



