﻿====================
Acerca de IMUT 
==================== 
.. image:: imut-web/intro-imut.png
   :height: 900 px
   :width: 1600 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   

====================
IMUT Web
====================
.. image:: imut-web/portada.png
   :height: 900 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Inicio de Sesión
=================
En esta vista se podrá iniciar sesión si el usuario se encuentra registrado en el sistema.

.. figure:: imut-web/login.png
   :height: 400 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Login Web de IMUT
   
   Menú para inicio de sesión Web


Panel de control
================
Una vez identificado en el sistema, se dará acceso a la siguiente vista:

.. figure:: imut-web/dashboard.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Dashboard
   
   Resumen Operacional de IMUT

A continuación describimos los menús que se encuentran en la parte izquierda. Una vez identificado en el sistema, se dará acceso a la siguiente vista:

.. figure:: imut-web/menu-lateral.png
   :height: 900 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú de Administración y Configuraciones
   
   
Reportes
========
Reporte Operacional
-------------------
Reporte que permite visualizar:

1. Cantidad de inspecciones realizadas por estatus(Aprobadas, Canceladas, Rechazadas, Pausadas)
2. Inspecciones realizadas desglosados por día de la semana (L-D)
3. Cantidad de inspecciones realizadas por tipo de carga

.. figure:: imut-web/reportes.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Reporte de operación del sistema de inspecciones
   
   Este reporte puede ser filtrado por fechas y zonas de inspección

Bitácora de Inspecciones
------------------------
En esta sección se podrá observar las inspecciones que se han realizado, consultado mediante el filtro de fechas y detalles, de igual manera se podrá ver el reporte ademas permitirá descargarlo en formato PDF.

.. figure:: imut-web/bitacora-inspecciones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Administración y Configuración de inspecciones
   
Inspecciones
============
.. _imut-inspecciones:

Inspecciones
------------
Administración del catálogo de inspecciones, se podrán crear, editar y clonar inspecciones, así como añadir personal interesado a las inspecciones.

.. figure:: imut-web/inspecciones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Administración y Configuración de inspecciones
 
 
.. _imut-puntos:

Puntos de Inspecciones
----------------------
Administración del catálogo de puntos de inspección, usted podrá agregar, editar activar y desactivar puntos de inspección.

.. figure:: imut-web/inspecciones-puntos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catalogo de puntos de inspección
   
Tipos de Carga
--------------
Administración del catálogo del tipos de carga, usted podrá agregar, editar, activar y desactivar los tipos de carga.

.. figure:: imut-web/cargas-tipos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catalogo de tipos de carga
   
Usuarios
========
Usuarios del Sistema
--------------------
Administración del catálogo del usuarios, usted podrá agregar, editar, cambiar la contraseña y activar o desactivar a los usuarios que podrán tener acceso al sistema Web.

.. figure:: imut-web/usuarios-sistema.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú de Administración de usuarios
   
Usuarios Interesados
--------------------
Administración del catálogo del usuarios interesados, usted podrá agregar, editar, cambiar la contraseña y activar o desactivar a los usuarios interesados, los cuales podrán ser agregados a las inspecciones para recibir notificaciones de las inspecciones agregadas.

.. figure:: imut-web/usuarios-interesados.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catalogo de contactos para recibir notificaciones

.. _imut-revisores:

Revisores
---------
Administración del catálogo de revisores, usted podrá agregar, editar, cambiar la contraseña activar, desactivar y asignarlos a zonas, para poder observar las inspecciones asignadas a esas zonas, los revisores para realizar las inspecciones en la aplicación móvil.

.. figure:: imut-web/usuarios-revisores.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de revisores para inspección
   
Embarques
---------
Administración del catálogo del personal de Embarques/Supervisores, usted podrá agregar, editar, activar y desactivar los supervisores que autorizaran el término de la inspección.

.. figure:: imut-web/usuarios-embarques.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catalogo de usuarios para autorizar embarques
   
Vehículos y Choferes
====================
.. _imut-fleteras:
   
Empresa Fletera
---------------
Administración del catálogo de empresas fletera, usted podrá agregar, editar, activar y desactivar las empresas Fleteras, las cuales pertenecen los vehículos y los choferes.

.. figure:: imut-web/empresa-fletera.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Empresas Fleteras

.. _imut-vehiculos:
   
Vehículos
---------
Administración del catálogo de Vehículos, usted podrá agregar, editar, activar y desactivar los vehículos para auto-completar los campos en la aplicación móvil, además de filtrar los vehículos por medio de Afiliación, Estatus, Tipo de Vehículo, Empresa Fletera y se realizara búsqueda sobre los datos filtrados.


.. figure:: imut-web/vehiculos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Vehículos para captura más rápida durante la inspección

.. _imut-choferes:

Choferes
--------
Administración del catálogo de choferes, usted podrá agregar, editar, activar y desactivar los choferes para autocompletar los campos en la aplicación móvil, además de filtrar los choferes por medio de Afiliación, Empresa y Estatus.

.. figure:: imut-web/choferes.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Choferes para captura más rápida durante la inspección
   
Configuraciones
===============
Afiliaciones
-------------
Administración de afiliaciones (altas, bajas e inhabilitaciones)

.. figure:: imut-web/afiliaciones.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de afiliaciones para usuarios tipo clientes

..note:: Esta sección solo sera visible para los usuarios con varias afiliaciones.

Zonas
-----
Administración de Zonas, en este catálogo se podrá dar altas, editar, activar y desactivar las zonas, además de filtrar por medio de Afiliación y Estatus.

.. figure:: imut-web/zonas.png
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Zonas de inspección
   
Configuración Afiliaciones
--------------------------
 En esta sección puede dar de alta los parámetros necesarios para que el sistema funcione correctamente.

.. figure:: imut-web/afiliaciones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Afiliaciones
   
Licencias, Dispositivos y Contrato
==================================
.. _imut-dispositivos:
   
Dispositivos
------------
Administración del catálogo de dispositivos, usted podrá agregar, editar activar y desactivar los dispositivos para poder acceder a la aplicación móvil mediante los contratos, además de filtrar por medio de Afiliación y Estatus.

.. figure:: imut-web/dispositivos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Dispositivos 


.. _imut-licencias:   

Licencias
---------
Administración de licencias asignadas a sus afiliaciones. Aquí puede consultar la lista de sus licencias y la vigencia de ellas

.. figure:: imut-web/imut-licencias.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Licencias 

   
.. _imut-contrato:   
   
Contratos
---------
Administración de contratos, usted podrá agregar y eliminar los contratos para poder acceder a la aplicación móvil, además de filtrar por medio de Afiliación.  mediante los dispositivos.

.. figure:: imut-web/contratos.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Contratos para asignar las licencias a dispositivos
   



