﻿====================
TagPatrol Registro
====================


Login
=====

Se iniciara sesión el con la misma cuenta de la página Web TagPatrol para poder registrar o editar 
una etiqueta al de la instalación, como se observa en la siguiente imagen.

.. figure:: tpRegistro/loginRegistrotp.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de inicio de sesión para el administrador.


Registro TAGs
==================
Una vez iniciada la sesión, la aplicación muestra una serie de opciones que permitirá registrar o editar
la etiqueta, como se observa en la siguiente imagen.


.. figure:: tpRegistro/inicio.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la opciones disponibles para el registro de una etiqueta o punto de revisión.

Actualizar Ubicación
====================
En esta opción sé podra editar la ubicación de la etiqueta, mostrando así la siguiente imagen.
 
.. figure:: tpRegistro/ubicacion.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la actualización de la ubicación.


	Paso 1. Se deberá seleccionar la instalación correspondiente del tag.

	Paso 2. Se deberá seleccionar el tag el cual se desea cambiar la ubicación.

	Paso 3. Finalmente se deberá hacer click en el botón de "Actualizar ubicación".

Una vez adentro mostrara la siguiente imagen.

.. figure:: tpRegistro/nuevaUbucacion.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el mapa en donde sé podrá seleccionar una nueva ubicación.


Para concluir se deberá mover en el mapa para poder asignar la nueva ubicación
y se dará click en "Seleccionarla ubicación" donde saldrá un mensaje, como se muestra en la siguiente
imagen.

.. figure:: tpRegistro/mensajeUbicacion.png
   :height: 400 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la confirmación de la nueva ubicación.

Registrar Fotos
===================

En esta sección se presentara la opción de poder agregar un foto de instalación o la ubicación relacionado con
la etiqueta o los puntos de revisión, como se muestra en la siguiente imagen.

.. figure:: tpRegistro/registro.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el registro de las fotos en las etiquetas o puntos de revisión.


	Paso 1. Se deberá seleccionar la instalación correspondiente del tag.

	Paso 2. Se deberá seleccionar el tag el cual se desea agregar una imagen.

	Paso 3. Finalmente se deberá hacer click en el botón con imagen de la cámara para así poder capturar la foto.

Escribir en la etiqueta
=======================
En esta opción el administrador podrá registra el Tag o verificar que se muestre correctamente

.. figure:: tpRegistro/escribir.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la lectura y escritura de la etiqueta o el punto de revisión.


	Paso 1. Se deberá seleccionar la instalación correspondiente.

	Paso 2. Se deberá seleccionar la etiqueta que se desea registrar, modificar o verificar.

	Paso 3. Finalmente, en caso de registrar o modificar la etiqueta, se dará click en el botón "Escribir en la Etiqueta", como se muestra en la siguiente imagen.


.. figure:: tpRegistro/actualizarEtiqueta.png
   :height: 400 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra mensaje para poder escribir en la etiqueta.


En caso de requerir el nombre de la etiqueta se dará click en el botón de "Leer desde la etiqueta", como se muestra en la siguiente imagen.


.. figure:: tpRegistro/leerEtiqueta.png
   :height: 400 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Mensaje para verificar el nombre del Tag.




