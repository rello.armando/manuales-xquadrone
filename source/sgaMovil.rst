﻿===================
SGA Móvil
===================


Inicio de sesión
================

Al iniciar la aplicación se mostrará la imagen que se muestra a continuación para poder iniciar sesión solicitando el Usuario y Contraseña: 

.. figure:: sga-movil/login.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Login Móvil de SGA
  

Al ingresar con información errónea los mensajes que se muestran son los siguientes: 

.. figure:: sga-movil/login-movil-error.jpg
   :height: 200 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
   Error inicio de sesión


.. note:: Si el problema persiste contactar al administrador.
 

       
Tipo de Pases
==============

Pase Extraordinario
====================
Es un pase provisional que otorga el administrador del sistema en donde permiten al trabajador tener acceso o salir de la planta, sin tener que registrar el vehículo o herramientas que posee.


.. figure:: sga-movil/paseExtra.png
   :height: 900 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
   Trabajador con pase Extraordinario

Pase Normal
====================
En este tipo de pases otorga el administrador aquellos trabajadores por fuera de la planta ya se contratistas o proveedores.

.. figure:: sga-movil/PaseNormal.png
   :height: 900 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Trabajador con pase Normal

Inicio
=======
En la vista inicial se puede ver listo para solicitar el escaneo del código QR para el pase como se muestra a continuación:

.. figure:: sga-movil/scanearCodigo.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Escaneo del código QR  

Escaneo de Pases
================
Al seleccionar la imagen la siguiente función del sistema es abrir la cámara para escanear el código QR necesario o en la parte superior se puede seleccionar la opción de buscar por número de pase como se muestra a continuación:

.. figure:: sga-movil/EscaneRealizado.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Escaneo de código QR


.. figure:: sga-movil/numPase.png
   :height: 400 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Búsqueda por número de pase.


Ingreso a la Planta
===================

Una vez que sea escaneado el código QR de un pase que se haya probado con anterioridad se muestra la información de los trabajadores que se encuentran asignados al pase indicando que trabajadores se encuentran dentro de la planta, los que salieron y las solicitudes de nuevo ingreso como se muestra en la  siguientes imágenes.

.. figure:: sga-movil/Adentro.png
   :height: 100 px
   :width: 100 px
   :scale: 50 %
   :alt: alternate text
   :align: center
  
   Dentro de la planta

.. figure:: sga-movil/Afuera.png
   :height: 100 px
   :width: 100 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Fuera de la planta

.. figure:: sga-movil/AccederPase.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Trabajador Pase Normal

.. figure:: sga-movil/paseExtra.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Trabajador Pase Extraordinario

Cuando se requiere realizar un ingreso a la planta, se tiene que seleccionar a la persona que está solicitando entrada y que está dentro del pase, en caso de ser "pase normal" al seleccionarla se mostrara las opciones de: Ver detalle de la persona y aceptar contacto como se muestra a continuación:

.. figure:: sga-movil/aceptarEntrada.png
   :height: 400 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Detalle en pase extraordinario

En la opción de ver detalle se puede visualizar la información del trabajador que ingresará, Datos personales, Datos de la empresa, Documentación relacionada al trabajador.

.. figure:: sga-movil/detalles.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center


   Datos personales.

.. figure:: sga-movil/empresa.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text   
   :align: center


   Datos de la empresa.

.. figure:: sga-movil/reportes.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Documentación relacionada al trabajador.

En el último apartado de información de documentación del trabajador se puede visualizar la documentación que se encuentra dada de alta, solo se tiene que seleccionar el documento que se desea verificar y se mostrara una vista previa del documento como se muestra a continuación:

.. figure:: sga-movil/Documento.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center


   Detalle de documentos


Al seleccionar la Opción de "+", se muestra el estatus del trabajador, ya sea cancelar la entrada o aceptar la entrada.

.. figure:: sga-movil/Vermas.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Estatus del trabajador

Una vez que se seleccione la opción de color verde se realiza el cambio de estatus como se muestra a continuación:


.. figure:: sga-movil/SalidaAceptada.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Cambio del estatus

Se permite seleccionar las herramientas con las que se va a ingresar o salir de la planta al trabajador como se muestra a continuación:

.. figure:: sga-movil/herramientas.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Selección de las herramientas.

También se deberá generar la autorización del vehículo con el que ingresara o saldrá el trabajador de la planta como se muestra a continuación:

.. figure:: sga-movil/VEHICULO.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Revisión del vehículo.

Visualización de entradas y salidas
====================================
Una vez que sea aceptado el trabajador para entrar en planta, se muestra la información de trabajadores en planta, trabajadores fuera de planta, personas con acceso cancelado, personas ausentes, equipo dentro y fuera de planta, vehículos dentro, fuera y cancelado



Salida de trabajador
====================
Para realizar la salida de un trabajador solo se tiene que seleccionar el trabajador del pase y seleccionar de aceptar salida.

.. figure:: sga-movil/salidaExtra.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   cuando se acepta una salida en pase Extraordinario


.. figure:: sga-movil/seleccion.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla generada cuando se acepta una salida en pase Normal


Una vez que se seleccione la salida del trabajador en el caso de ser un pase extraordinario se muestra en un mensaje para verificar si se desea modificar el estatus del trabajado, como se muestra a continuación:

.. figure:: sga-movil/aceptarSalida.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla genera el cambio de estatus del trabajador en pase Extraordinario




.. figure:: sga-movil/aceptarSalidaExtra.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de las entradas y salidas en pase Extraordinario

Si es un pase normal se genera la salida directamente.

.. figure:: sga-movil/Aceptar.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla genera salida del trabajador en pase Normal

Funcionalidad especial Supervisor
=================================
Cuando un supervisor este dentro del sistema móvil, este podrá realizar solamente amonestaciones de los trabajadores indicando cual fue la causa de la amonestación, cuando esta se de alta, se verá reflejado dentro del pase de trabajador como se muestra a continuación:


.. figure:: sga-movil/amonestar.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla generada con amonestaciones. 


Ayuda adicional
================

Permisos por rol
	o Móvil Supervisor: Tiene derecho a amonestar a los trabajadores.

 	o Móvil No supervisor: Tiene derecho a visualizar los pases y aceptarlos, así como ver las cantidades de personas, Equipo, Vehículos, que salen entran.

Preguntas frecuentes:

	¿Se puede aceptar un pase sin ser aceptado por un administrador?
		R: No, todo pase tiene que ser aceptado por el administrador.
	¿Por qué mi usuario no puede amonestar a los trabajadores?
		R: Solamente el rol de supervisor puede dar de alta una amonestación a un trabajador.
	¿Cómo puedo saber que es el trabajador correcto?
		R: Existe la opción de verificar información del trabajador donde muestra datos personales, así como la foto del trabajador en caso de que hayan agregado la foto por parte del administrador.
	¿Cómo puedo saber si hay personas dentro de la planta?
		R: Se puede ver la cantidad de personas que entran y salen de la planta en la opción.
	¿Cómo puedo saber cuál es mi id de pase?
		R: El administrador deberá proporcionar el código como el id del pase.


Si desea obtener apoyo técnico o ayuda en relación con el software, póngase en contacto con el equipo de Xquadrone Si desea obtener apoyo técnico o ayuda en relación con el software, póngase en contacto con el equipo de Xquadrone https://www.xquadrone.mx/, tel.8007881762, 6865645871.



