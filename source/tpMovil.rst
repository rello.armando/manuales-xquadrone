﻿==================
TagPatrol Móvil 
==================


Login
======
Inicio en aplicación como empleado:
Login en aplicación Tag patrol. El empleado (Guardia de seguridad) deberá de ingresar el
número de empleado para iniciar sesión, como se observa en la figura.

.. figure:: tp-movil/tp-login.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de inicio de sesión para empleado.



Pantalla de Inicio
==================
Una vez iniciada la sesión, la aplicación muestra la ronda que se tiene que
realizar en el momento, se observa en figura. 


.. figure:: tp-movil/Ronda.png
   :height: 875 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la ronda disponible para realizarse.




Tipos de Rondas
================

Rondas Normal o por Horario
----------------------------

En las rodas normales o por horario estara disponible unicamente en el horario que se encuentra configurado
previamente en la pagina web.

.. figure:: tp-movil/rondaNormal.png
   :height: 875 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra ronda normal o por horario


Rondas Seleccionables
---------------------

Al momento de iniciar sesion se mostrara en la pantalla las rondas que se encuentran disponibles configuradas dentro de la pagina web Tag Patrol.

.. figure:: tp-movil/seleccionable.png
   :height: 875 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra rondas seleccionables.

Rondas Libres
--------------

Aparece al momento de iniciar sesión, mostrando los puntos de revisión los cuales se podrá elegir para poder realizar la revisión 

.. figure:: tp-movil/rondalibre.png
   :height: 875 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra ronda libre.

Inicio de la Ronda
===================
Para iniciar la ronda se requiere presionar el botón de iniciar ronda, se observa en la figura. Enseguida muestra los lugares que tiene que revisar pintando el punto en color verde.
El empleado puede ver el nombre del lugar solo tendrá que tocar el punto, enseguida resaltará el nombre, como se observa en la figura. El sistema permite registrar hallazgos en ronda y en tránsito. Los hallazgos en ronda son las anomalías que se localizan en cada etiqueta.
Hallazgos en tránsito estos son las anomalías que se localizan fuera del lugar de revisión, durante el trayecto de una etiqueta a otra.


.. figure:: tp-movil/iniciarRonda.png
   :height: 875 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el lugar a revisar (etiqueta color verde).
   
.. note:: Para revisar las tareas de la etiqueta se requiere presionar el botón "Lectura de Tag" para activar el sensor NFC.


Lectura de tags
================
Despues de presionar el boton de "lectura de tag", aparece la pantalla "acerque el dispositivo al tag" donde se puede proceder a la lectura de la etiqueta.

.. figure:: tp-movil/LecturaNFC.png
   :height: 875 px
   :width: 425 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el aviso para acercar el dispositivo a la etiqueta.

CheckList
=========
En checklist, el Coordinador puede agregar tareas a etiquetas específicas, mediante la interfaz mostrada. Filtrando por instalación como mostrado en la Figura 2, el coordinador puede seleccionar una etiqueta en el elemento correspondiente, y una vez seleccionada puede filtrar las tareas por Categoría, Sub Categoría y Grupo.

En el grupo de elementos de Asignar Tareas puede registrar nuevas tareas al punto seleccionado.

Seleccionar una etiqueta da como resultado un listado de tareas como en la Figura 12.
Al seleccionar una tarea puede modificar sus atributos o eliminarla como se muestra en la Figura 13. 

.. figure:: tp-movil/SeleccionarPreguntas.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra preguntas revisadas


Tareas de Tag
=============
Despues de leer la etiqueta, aparece la lista de preguntas que se pueden contestar ✗ ó √, en caso de no existir anomalías se contestará con √. Si existen se selecciona ✗, y enseguida se abre una ventana para registrar hallazgos. 

.. figure:: tp-movil/SeccionPreguntas.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra lista de preguntas para el lugar llamado “Pruebas”


Registrar hallazgo 
==================
Al contestar una de las preguntas con ✗ inmediatamente aparece un dialogo para el registro del hallazgo, donde se pueden poner comentarios y hasta 3 fotos de evidencia. 

.. figure:: tp-movil/RegistrarHallazgo.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla donde se registra hallazgo en ronda
   

.. figure:: tp-movil/cometario.png
   :height: 875 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Campos para registrar comentarios a hallazgos


Una vez se llenan los campos de descripción de hallazgo se presiona el botón enviar, para registrar la información, el empleado tiene la opción de notificar el hallazgo a su supervisor, enseguida se muestra un segundo mensaje donde al empleado se le da la opción de realizar una llamada al supervisor, como se detalla a continuación.

  
Hallazgos repetidos
===================
En caso de que existan hallazgos abiertos que se han registrado, aparecerán en una lista.

.. figure:: tp-movil/ListaHallazgo.jpg
   :height: 875 px
   :width: 775 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra lista de hallazgos abiertos del lugar a revisar.

Enviar hallazgo
===================

.. figure:: tp-movil/MensajeHallazgo.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
   Al registrar un hallazgo este podrá ser enviado al presionar el botón "enviar"


.. note:: Es posible marcar al supervisor después dar en opción enviar datos de hallazgo, desde la lista de contactos configurada en TagPatrol web. 

.. figure:: tp-movil/LlamarSupervisor.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center



Visualización de Hallazgos
==========================
El usuario administrador puede ver los hallazgos como se observa en la figura.

.. figure:: tp-movil/sesion-super3.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center 


   Pantalla donde se muestra la descripción de hallazgo

   

Login Supervisor
================
El Coordinador puede visualizar los hallazgos de la instalación en la vista de supervisor, a la cual se puede acceder deslizando la pantalla a la izquierda, en esta vista se encuentra la interfaz "Login Supervisor". El acceso a esta sección solo esta permitido para usuarios web con las mismas credenciales.


.. figure:: tp-movil/LoginSupervisor.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Login Supervisor

En esta vista es posible cambiar entre hallazgos en Ronda ó hallazgos en Transito

.. figure:: tp-movil/sesion-super2.png
   :height: 875 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Detalle de hallazgos


Hallazgos desde el móvil
------------------------
En este apartado se puede revisar desde la app los últimos hallazgos registrados permitiendo a el supervisor hacer una consulta desde el móvil.

.. figure:: tp-movil/sesion-super1.png
   :height: 875 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Hallazgos desde el móvil
   
