======================
Acerca de TagPatrol 
======================
.. image:: tp-web/intro-tp.png
   :height: 900 px
   :width: 1600 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   

Versiones de TagPatrol
======================
En tagPatrol existen 3 tipos de funcionamiento para la realización de rondines:
             ● Ronda Normal (por horario configurado)
			 
             ● Ronda seleccionable
			 
             ● Ronda Libre

Ronda Normal
-----------------------
En este tipo de rondas de debe configurar el tablero de horarios para indicar los días y las horas en las cuales se deben realizar las rondas. En la aplicación móvil se abre la ronda que corresponde al horario.

Ronda Seleccionable
-----------------------
TagPatrol Ronda Seleccionable hace más flexibles las rondas normales permitiendo seleccionar una de las rondas horarias que se desea hacer de una lista a cualquier hora y día de la semana.

Ronda Libre
-----------------------
TagPatrol Ronda Libre es la versión que permite generar una lista de las etiquetas de la instalación, de esta manera se pueden escanear una por una en cualquier momento y en cualquier orden. En esta versión aunque se tiene que seleccionar las etiquetas de la ronda libre, no es necesario seguir una ruta predeterminada.

Para configurar el tipo de ronda que se activara en el dispositivo, se cambia la versión en la sección CONFIGURACION / DISPOSITIVOS y se edita el dispositivo para ver el menú de la imagen. En el próximo inicio de sesión el dispositivo entrara en el modo seleccionado.

   
===================
TagPatrol Web
===================

Inicio
====== 
El usuario ingresará a la aplicación Web, donde el usuario ingresará
su Usuario y Contraseña, para después seleccionar el botón de "Iniciar", para entrar al sistema en caso de que los datos sean correctos.


.. figure:: tp-web/login.png
   :height: 800 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Login de Tag Patrol 
   
   Menú para inicio de sesión 

.. hint:: En caso de que el usuario haya olvidado su contraseña puede seleccionar la opción de recuperar contraseña

Una vez iniciada la sesión se le mostrará al cliente un resumen de los hallazgos, así como un menú lateral para acceder a las diferentes funciones del sistema.
Así mismo, cuenta con la opción de exportar el resumen en un documento de Excel en un botón dedicado con el ícono de Excel.
Además, cuenta con una barra de búsqueda para hallazgos.


.. figure:: tp-web/inicio.png
   :height: 900 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Inicio
   
   
   
Rondas
=======

.. figure:: tp-web/menu-rondas.png
   :height: 400 px
   :width:  500 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú de Rondas


Bitácora
--------
La sección de bitacora de rondas cuenta con la habilidad de consultar rondas dentro de un periodo determinado. El usuario puede seleccionar el rango de tiempo, y una vez seleccionados, presionando el botón de “Consultar” se muestran los resultados.
Además, cuenta con un botón para Imprimir el resultado y otro marcado como “Exportar” para generar un documento de Excel con los mismos resultados.


Configuración de Rondas
-----------------------
Crear horario
^^^^^^^^^^^^^
En Crear horario, se pueden consultar y crear horarios, seleccionando una hora inicial y final, no sin antes filtrar por instalación.

.. figure:: tp-web/rondas.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Horarios
  
 
En Crear ronda, el usuario puede consultar y registrar nuevas rondas, después de haber seleccionado una instalación.

.. figure:: tp-web/nueva-ronda.png
   :height: 800 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Crear Ronda

Crear Ruta
^^^^^^^^^^^^^
En Crear rondín, después de filtrar por instalación, puede crear, visualizar y modificar rutas. Relacionando etiquetas con rondas especificas se pueden crear rondines.

.. figure:: tp-web/rondas-ruta.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Crear Ruta
   


Configuración Rondas Libres
---------------------------
Para configurar las rondas libres es necesario generar una lista de etiquetas que esten registradas en 
la instalacion. En este tipo de ronda no importa en orden de las etiquetas, ya que se pueden 
leer de una por una etiqueta y hacer la revision en cualquier orden y en cualquier momento. 


Consulta
--------
En esta seccion se pueden graficar los hallazgos para poder generar en un reporte mensual


Hallazgos
=========
En la sección de Hallazgos, ya sea en ronda o en tránsito, se puede obtener un resumen de hallazgos. 
Además, cuenta con opciones para consultar hallazgos por tiempo (seleccionando desde que tiempo y hasta que tiempo consultar), por grupo, por estatus y filtrarlos en caso de estar repetidos. Cuenta con un botón con el ícono de Excel para exportar los resultados en un documento en ese formato.

.. figure:: tp-web/hallazgos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
   Pantalla de hallazgos en ronda



Reportes
========
En la sección de Reportes se pueden generar diferentes reportes de acuerdo con un intervalo de tiempo seleccionado. Con los elementos para seleccionar fechas y hora marcados como Desde y Hasta, se puede seleccionar el intervalo para generar los reportes. Una vez que el intervalo sea seleccionado, el usuario puede generar reportes en PDF, y Fotos en el caso de los Hallazgos, seleccionando el botón con el texto correspondiente.

Se cuentan con 4 tipos de Reportes:

   Reporte Check List: Un reporte general de los resultados de las checklists en las rondas. Puede ser filtrado por positivos y negativos (Sí y no).

   Bitácora de ronda: Un reporte general de las rondas programadas en el tiempo seleccionado. Puede ser filtrada por sólo rondas con hallazgos, sólo rondas incompletas ó sólo rondas no ejecutadas.

   Hallazgo en ronda: Un reporte general de los hallazgos registrados en ronda.
   Pueden ser filtrados por Abiertos, En progreso, No atendidos, Cerrados y Repetidos.

   Hallazgo en tránsito: Un reporte general de los hallazgos registrados fuera de lo establecido en ronda. Pueden ser filtrados por Abiertos, En progreso, No atendidos y Cerrados.

.. figure:: tp-web/reportes.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de reportes


Incidentes
===========
En la pantalla se podrá levantar un reporte de incidente al ocurrir un incidente dentro del patrullaje.

.. figure:: tp-web/incidente.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de inicio de coordinador
   
Incidentes
-----------
En esta sección se realizara los reportes cuando ocurren un incidente dentro del patrullaje.


Búsqueda de Incidentes
----------------------   
En esta sección se mostrarán la lista de incidentes registrados, las cueales se podra filtrar por medio de un rango de fechas y por el estatus de incidentes.


Tags
====


.. _tp-tags:

Etiquetas 
----------
En la sección de etiquetas, el usuario puede visualizar en un mapa las etiquetas registradas, filtradas por la instalación seleccionada. 
Además, muestra una tabla con las etiquetas registradas, con la opción para editarlas y eliminarlas.
Por último, puede agregar nuevas etiquetas en la opción de Nueva Etiqueta, llenando los campos mostrados y presionando el botón de Agregar.

.. figure:: tp-web/tags.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Interfaz de etiquetas
   

.. figure:: tp-web/tags-agregar.png
   :height: 800 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Agregar etiqueta
   
   
.. _tp-tareas:

Checklist
---------
En checklist, el Coordinador puede agregar tareas a etiquetas específicas, mediante la interfaz. 
Filtrando por instalación, el coordinador puede seleccionar una etiqueta en el elemento correspondiente.
En el grupo de elementos de Asignar Tareas puede registrar nuevas tareas al punto seleccionado.
Seleccionar una etiqueta da como resultado un listado de tareas.
Al seleccionar una tarea puede modificar sus atributos o eliminarla como se muestra en
la siguiente imagen.

.. figure:: tp-web/tags-checklist.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Interfaz de checklist
   

.. figure:: tp-web/tags-checklist-borrar.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Edición y eliminación de tareas

Categorías y Subcategorías
--------------------------  
En esta sección se mostrara la categorías y subcategorías en donde permitirá agregar, editar y eliminar.

Configuración
=============
.. _tp-usuarios:

Usuarios
--------
La sección de Usuarios cuenta con una lista donde puede visualizarlos de acuerdo con la instalación seleccionada,
El usuario también puede agregar nuevos Usuarios, editar nombre y password, así como las instalaciones a las cuales este usuario tiene acceso.

.. figure:: tp-web/usuarios.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Administración de Usuarios
   
.. _tp-empleados:
   
Empleados
---------
La sección de empleados cuenta con una lista de empleados, donde puede visualizarlos de acuerdo con la instalación seleccionada. El usuario también puede agregar nuevos empleados mediante la interfaz mostrada en la siguiente imagen, llenado los campos y presionando el botón Guardar.

.. figure:: tp-web/empleados.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Administración de empleados
   

.. figure:: tp-web/nuevo-empleado.png
   :height: 800 px
   :width: 800 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Interfaz para agregar nuevo empleado
   

Contactos de Hallazgos
----------------------
En Contactos el usuario es capaz de consultar y agregar contactos, y de la misma manera, selecciona la instalación en la que desea realizar la consulta o adición. El usuario visualiza los contactos registrados para la instalación seleccionada. Además, se añaden contactos al llenar los campos y presionar el botón Registrar.

.. figure:: tp-web/contactos.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Lista de contactos

.. _tp-dispositivos:
   
Dispositivos
------------
La sección de dispositivos permite visualizar los dispositivos, seleccionando por instalación similar a la imagen mostrada a continuación. También se cuenta con la función de agregar nuevos dispositivos, llenando la información mostrada y presionando Guardar. En caso de querer cancelar, presionar Cancelar.

.. figure:: tp-web/dispositivos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Lista de dispositivos

   
Áreas, Departamentos y Categorías
---------------------------------
Catálogos para dar de alta las Áreas, Departamentos y Categorías para el reporte de incidentes.

Área
^^^^
Catalogo para crear una nueva área, editarla o borrarla del sistema.

Departamentos
^^^^^^^^^^^^^
Catalogo para agregar departamentos , editarlos o borrarlos del sistema.

Categorías Incidentes
^^^^^^^^^^^^^^^^^^^^^
Permitirá la administración de categorías pertenecientes al Tipo de incidente, se podrá realizar las siguientes acciones: Agregar, Editar y Eliminar.



Puestos
-------
Catalogo de puestos para el reporte de incidentes, ademas permitirá acciones como: Agregar un nuevo puesto, editar o borrar del sistema.
 
Administración
==============
Correos
-------------------------------
En esta sección se administran los correos de notificación, los cuales se asignan a un grupo de ponderación para el envío de hallazgos de acuerdo a su nivel de impacto, ademas se podrá Agregar nuevos días de atención para un grupo permitiendo editar y borrarlo del sistema.


Instalaciones
=============
Servicios permite al usuario consultar, agregar, modificar y eliminar servicios. Se pueden agregar servicios seleccionando una Empresa, llenando los campos de Nombre y Clave, seleccionando una Zona y presionando el botón Agregar.

.. figure:: tp-web/instalaciones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Editar instalación
   
   
Además, seleccionando uno de los servicios listados, puede editarlo o eliminarlo. Al editarlo se mostrará la interfaz, donde puede editar los campos y guardarlos.

Contacto de licencias
---------------------
En este apartado se capturan los correos electrónicos de contacto para avisos de actualizaciones de sistema,permitiendo agregar, modificar y borrarlo del sistema. 

.. _tp-licencias:

Licencias
---------
En Licencias, el Administrador puede consultar sus licencias asignadas con sus atributos.

.. figure:: tp-web/licencias.png
   :height: 900 px
   :width: 1100 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Listado de licencia

   Además, puede consultar sus dispositivos con respecto a las licencias asignadas y no asignadas.


Por último, el Administrador puede Asignar Licencias a Dispositivos, seleccionando los elementos requeridos y presionando Asignar.

.. figure:: tp-web/licencia-asignar.png
   :height: 750 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Asignar licencia a dispositivo
   
.. Quite esta sección para evitar confusiones a los usuarios - AR

.. Empezar ------- En esta sección se podrá llevar el procedimiento de crear a un nuevo cliente siguiendo un flujo de proceso. Se creara a un nuevo clientes, la empresa, el servicio, el dispositivo, una nueva licencia y finalmente se confirmara para así poder crear un nuevo cliente.
