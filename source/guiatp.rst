﻿====================
GUÍA RÁPIDA TP
====================

Registro de Usuarios, Dispositivos, Licencias
=============================================
+ Los usuarios web se registran en la sección Configuración/:ref:`tp-usuarios`
	- Presionar el botón agregar usuario
	- Llenar los campos "Nombre, Apellidos, Nombre de usuario, contraseña y correo"
	- Seleccionar el tipo de usuario

+ Los empleados se podrán registrar en la sección Configuración/:ref:`tp-empleados`
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón agregar empleado
	- Llenar los campos "Numero, Nombre, Apellidos"
	- Seleccionar el tipo de empleado "Guardia ó Supervisor"

+ Los dispositivos se podrán registrar en la sección Configuración/:ref:`tp-dispositivos`  
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón agregar dispositivo
	- Llenar los campos "Marca, Modelo, Identificador y Versión de TagPatrol"
	
+ Las licencias se administran en la sección Administración/:ref:`tp-licencias` 
	- En la pestaña licencias, donde aparecen las que ha adquirido, así como su vigencia
	- En Dispositivos podrá dar de alta estos según lo descrito anteriormente
	- En Asignar licencias a dispositivos, podrá vincular la licencia a un dispositivo	
	
Registro de Tags y Tareas
=========================
+ Para dar de alta etiquetas en Tags/:ref:`tp-tags` 
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón agregar etiqueta	
	- Capturar el nombre de la etiqueta y el nivel: "Piso base, Piso 1, etc."
	
.. warning:: Es necesario que cada etiqueta tenga al menos una tarea programada Tags/:ref:`tp-tareas` 


Registro de Ronda
=================
Para la configuración de rondas le mostramos una animación con los pasos

.. figure:: tp-web/crear-ronda.gif
   :height: 800 px
   :width: 940 px
   :scale: 75 %
   :alt: alternate text
   :align: center

   Configuración de rondas en TagPatrol

