﻿========================
Acerca de DronePatrol 
========================
.. image:: dp-web/intro-dp.png
   :height: 900 px
   :width: 1600 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
====================
Drone Patrol Web
====================
	
.. image:: dp-web/portada.png
   :height: 1000 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Login
=====
En esta vista se podrá iniciar sesión si el usuario se encuentra registrado en el sistema, como se observa en la siguiente imagen.

.. figure:: dp-web/inicio-sesion.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Inicio de Sesión de Drone Patrol

.. error:: El usuario y/o la contraseña no son las correctas. Si el problema persiste contactar con el administrador.

.. hint:: Recuperación de contraseña. Si el usuario no cuenta con su contraseña no podrá iniciar sesión y se tendrá que usar
   el botón para recuperar su contraseña. Una vez escribiendo el correo con el cual fue dado de alta se podrá recibir
   este correo que será para restablecer la contraseña.

.. figure:: dp-web/recuperar-password.png
   :height: 800 px
   :width: 800 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Recuperación password de Drone Patrol
   

Una vez iniciada sesión se podrá acceder a la instalación que se le haya asignado, así como ver las misiones que se han realizado la última semana, y una tabla con las misiones hechas con el dron recientemente.
Con la siguiente información en Reporte:

	+------------------------+------------------------+
	| **Elementos**          | **Descripción**        |
	+------------------------+------------------------+
	| ID                     | Número ID de Drone     | 
	+------------------------+------------------------+ 
	| Misión                 | Nombre de Drone        | 
	+------------------------+------------------------+
	| Fecha                  | Marca de Drone         |
	+------------------------+------------------------+
	| Inicio                 | Modelo de Drone        |
	+------------------------+------------------------+
	| Finalizo               | Numero de serie        |
	+------------------------+------------------------+
	| Ver                    | Numero de serie        |
	+------------------------+------------------------+	


.. figure:: dp-web/misiones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla de inicio para cliente

Permitiendo seleccionar un misión para así poder abrir el reporte, de igual manera sé podrá observar en el mapa los puntos recorridos por el Drone.

CATALOGOS
==========

Usuarios
=========

.. figure:: dp-web/usuarios.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Configuración de usuarios y pilotos
   
.. _dp-usuarios:   

Administración de usuarios
--------------------------
En esta sección se podrá dar de alta aquellos usuarios autorizados para entrar al sistema, así como asignarles la instalación a la que pertenecen y los privilegios con los que contara, ademas permitirá buscar un usuario en la tabla de registros.

.. _dp-pilotos:   

Administración de pilotos
-------------------------
En esta sección se podrán registrar a los pilotos que se han dado de alta en el sistema, para que puedan usar la aplicación de Drone Patrol y poder realizar las misiones asignadas al dispositivo.


.. _dp-drones:

Drones
======
En esta sección se podrán administrar los drones que se usarán para volar en Drone Patrol.

.. figure:: dp-web/drones.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catálogo de Drones 
   

Drones
------
En esta sección sé podrá llevar a cabo el registro de los drones ubicado den el botón "Agregar Drone", en donde se agregara los siguientes datos:
   
+------------------------+--------------------------+
| **Elementos**          | **Descripción**          |
+------------------------+--------------------------+
| Número                 | Número ID de Drone       |
+------------------------+--------------------------+
| Dron                   | Nombre de Drone          |
+------------------------+--------------------------+
| Marca                  | Marca de Drone           |
+------------------------+--------------------------+
| Modelo                 | Modelo de Drone          |
+------------------------+--------------------------+
| Serial                 | Numero de serie          |
+------------------------+--------------------------+
| Fecha de compra        | Fecha de compra de Drone |
+------------------------+--------------------------+
| Cliente                | Nombre del cliente       |
+------------------------+--------------------------+
| Foto Drone             | Fotagrafia del drone     |
+------------------------+--------------------------+ 
	


También permitirá realizar las siguientes acciones:

1. Mostrar Foto.
2. Editar.
3. Activar/Desactivar.
4. Eliminar.
5. Filtrar por los Activos/Inactivos.


Marcas - Modelos
----------------
En esta sección se podrá mostrar los drones registrados, lo cual se buscara por medio de su marca y modelo ademas permitirá agregar nuevas marcas o modelos, también podrá eliminar una marca o un modelo.
  
.. _dp-dispositivos:

Dispositivos
============
En esta sección se podrán registrar y buscar los dispositivos Android donde instalarán la aplicación de Drone Patrol para realizar las misiones autónomas con drones.

.. figure:: dp-web/dispositivos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Configuración de Dispositivos 
   
Y cuenta con la siguiente información:

	+------------------------+------------------------+
	| **Elementos**          | **Descripción**        |
	+------------------------+------------------------+
	| Número                 | Número ID de Drone     | 
	+------------------------+------------------------+ 
	| IMEI                   | Nombre de Drone        | 
	+------------------------+------------------------+
	| Marca                  | Marca de Drone         |
	+------------------------+------------------------+
	| Modelo                 | Modelo de Drone        |
	+------------------------+------------------------+
	| Instalacion            | Número ID de Drone     | 
	+------------------------+------------------------+ 
	
También permitirá realizar las siguientes acciones:

1. Editar.
2. Activar/Desactivar.
3. Eliminar.
4. Filtrar por los Activos/Inactivos.

Contactos Alerta
================
En esta sección se darán de alta los correos para avisar de algún cambio en las licencias.

Licencias y Servicio
====================
.. _dp-licencias:

Licencias
---------
En esta sección se podrán asignar las licencias para los dispositivos autorizados para volar con Drone Patrol, se podrán editar, eliminar y crear nuevas licencias.

Dispositivos
------------
Las licencias se pueden asignar a dispositivos en específico, dándole click a asignar licencia y completando la información requerida.

.. figure:: dp-web/asignar-licencia.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú para asignar licencia a Dispositivos 
   
Historial
---------
En el historial de licencias, podemos visualizar las ediciones hechas, por acción, usuario y fecha.


Servicios
---------
Sección de administración de servicios Permite realizar búsquedas por medio de cliente y empresa, se llevara acabo el registro de nuevos servicios para darles al Drone Patrol.

Las acciones que se podrán realizar son las siguientes:

1. Agregar nueva empresa.
2. Editar empresa.
3. Eliminar empresa.


Misiones y Alarmas
==================
Si se selecciona una misión que se haya hecho en el mapa se mostrará los puntos por los
cuales pasó el dron, así como también se podrá seleccionar la opción ver, para ver el reporte
detallado de la misión.


Misiones
--------
Misiones
En esta sección se podrán agregar, editar, eliminar misiones, así como asignar las misiones a
los dispositivos, así como trazar los puntos de recorrido y agregar acciones.


.. figure:: dp-web/mision-add-punto.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Configuración de Misiones 
   
   
Alarmas
-------
En esta sección se podrán agregar, editar, eliminar alarmas, así como asignar las alarmas
a los dispositivos.


Bitácora
========
Misiones recorridas
-------------------
Bitácora de misiones recorridas.
Se mostrara el registro de todas las misiones que se han realizado, generando así una lista detallado donde sé podra "ver" los reportes generados.

.. figure:: dp-web/bitacora.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Bitácora de vuelos 
   
Pilotos
-------
En esta sección se muestra el piloto, las horas totales de la duración del vuelo y el dron que utiliza.

.. figure:: dp-web/pilotos.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Catalogo de pilotos 
   

