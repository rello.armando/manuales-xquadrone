﻿====================
GUÍA RÁPIDA DP 
====================

Registro de Dispositivos, Licencias y Drones
============================================
+ Los dispositivos se podrán registrar en la sección Catálogos/:ref:`dp-dispositivos`
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón agregar dispositivo
	- Llenar los campos "Identificador, Marca y Modelo"
	- Seleccionar la instalación  
	
+ Los Licencias se podrán registrar en la sección Licencias y Servicios/:ref:`dp-licencias`
	- Seleccionar el cliente, en caso de tener varias
	- En la pestaña licencias, aparecen las que se han adquirido, así como su vigencia
	- En la pestaña dispositivos podrá dar de alta estos según lo descrito anteriormente
	- En la botón "asignar licencias", podrá vincular la licencia a un dispositivo para activarlo
	
+ Los Drones se registrarán en Catálogos/:ref:`dp-drones`
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón agregar dispositivo
	- Llenar los campos "Marca, Modelo, Identificador y Versión de TagPatrol"
	
Registro de Usuarios y Pilotos
==============================
+ Los usuarios web se registran en :ref:`dp-usuarios` 
+ Los dispositivos se registran en la sección :ref:`dp-pilotos` 


Guía registrar Misiones
=======================
Para la configuración de misiones le mostramos una animación con los pasos

.. figure:: dp-web/dp-config-mision.gif
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Configurar misiones en DronePatrol
