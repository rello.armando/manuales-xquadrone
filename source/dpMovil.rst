====================
Drone Patrol Móvil
====================


Permite ejecutar las misiones pre configuradas de la pagina https://dronepatrol.mx/index.xhtml, 
esto facilita el monitoreo ya que DronePatrol hace vuelos autónomos con los drones



Inicio de Sesión
=================
El usuario y contraseña pertenece a un rol de piloto, este tiene pre-configurado las misiones
disponibles a ejecutar.


.. figure:: dp-movil/logindp.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
   Pantalla muestra el vista inicial.



Misiones
========
Una vez que se ingresa con un usuario válido, este muestra los datos del Drone conectado y se muestran los apartados INICIO y MISIONES, así como un botón para iniciar la misión, una vez seleccionada.

.. figure:: dp-movil/conectarDroneMisiones.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Botón para las misiones asignadas al Drone.

En la sección de Misiones están los recorridos disponibles para ejecutar.

.. figure:: dp-movil/listaMisiones.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la lista de Misiones


Una vez seleccionada la misión, se podrá iniciar la misión y esto mostrada el check-list de
pre-vuelo, que es un muy importante para validar que todo está correcto previo el vuelo.

.. figure:: dp-movil/preguntasInicio.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra las preguntas obligatorias para poder iniciar la Misión


Si todos los elementos del check-list están correctos y se presiona OK, entrada en modo visualización y listo para vuelo.

Visualización
==============
En la vista de visualización se puede observar en tiempo real el video tomado por la cámara del Drone, también el estatus, advertencias, niveles de batería y telemetría.

.. figure:: dp-movil/InicioMision.png
   :height: 400 px
   :width: 950 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el inicio de la misión

En la imagen siguiente se puede observar los sensores del Drone, estos indican si hay objetos cercanos con los que puede colisionar.


También se puede alternar en el modo mapa con el GPS en tiempo real de la unidad Drone, presionando el mapa miniatura de la parte inferior derecha.


.. figure:: dp-movil/mapa.png
   :height: 400 px
   :width: 950 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla mapa para verificar la ruta del Drone


Despegue
=========
Una vez todo listo y que está en un estado listo para volar (Ready to GO), se presiona el botón de inicio y la unidad hará un despegue (TakeOff). Indicando un mensaje de “Timeline Event is STARTED”.

.. figure:: dp-movil/despegue.png
   :height: 200 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el botón de despegue

Al finalizar la misión se debe verificar el estado del dron al momento de aterrizar.

.. figure:: dp-movil/preguntasFinal.png
   :height: 400 px
   :width: 950 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra la verificación del Drone

Bitácora
========
Una vez realizado el vuelo, este se registra en Pruebas y este se muestra
en una bitácora.

.. figure:: dp-movil/bitacora.png
   :height: 500 px
   :width: 1000 px
   :scale: 60 %
   :alt: alternate text
   :align: center

   Pantalla muestra el registro de la misión

Además la plataforma permite generar una reporte al cual se le puede adjuntar evidencia o imágenes importantes.

.. figure:: dp-movil/reporte.png
   :height: 500 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra el reporte de la misión

Advertencias
=============
.. warning:: Hay que recordar tomar precauciones al ejecutar estos vuelos autónomos, aquí exponemos los criterios para configurar una misión.


.. figure:: dp-movil/advertencia.png
   :height: 600 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Pantalla muestra las precauciones a seguir

Compatibilidad
===============
.. note:: Los siguientes modelos son compatibles con DronePatrol


.. figure:: dp-movil/modelo1.png
   :height: 400 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. figure:: dp-movil/modelo2.png
   :height: 400 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. figure:: dp-movil/modelo3.png
   :height: 400 px
   :width: 550 px
   :scale: 50 %
   :alt: alternate text
   :align: center
   
