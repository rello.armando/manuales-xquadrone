﻿====================
GUÍA RÁPIDA IMUT
====================
.. Estas son las secciones que estaban en la descripción de la tarea del Jira
.. Guía de inicio (Registro de dispositivo, licencia, usuarios móvil)
.. Alta de puntos de inspección 
.. Alta de inspecciones
.. Carga de catalogo

Registro de Dispositivos, Licencias y Revisores
===============================================
+ Los Dispositivos se podrán registrar en la sección Licencias y Dispositivos/:ref:`imut-dispositivos`
	- Presionar el botón "Agregar"
	- Agregar datos del dispositivo "Nombre, Marca, Modelo, Identificador"
	- Seleccionar la instalación  
	
+ Para activar un dispositivo móvil es necesario elaborar un contrato en el apartado Licencias y Dispositivos/:ref:`imut-contrato`
	- Seleccionar una instalación, en caso de tener varias
	- Presionar el botón "Agregar dispositivo"

+ Los Revisores se podrán registrar en la sección Usuarios/:ref:`imut-revisores`
	- Presionar el botón "Agregar" 
	- Capturar datos del guardia y la zona de inspecciones  "Afiliación, Zona, Nombre, Numero Empleado y Contraseña"
	- "Guardar"

Carga de Catálogos (Empresas Transportistas, Choferes y Unidades)
=================================================================
+ Los Catálogos de empresas transportistas se cargarán en Vehículos y Choferes/:ref:`imut-fleteras` 
	- Presionar el botón "Agregar"
	- Agregar datos "Nombre de la empresa fletera, Descripción y Afiliación"
	- "Guardar"
	
+ Los Catálogos Unidades se cargarán en Vehículos y Choferes/:ref:`imut-vehiculos` 
	- Presionar el botón "Agregar"
	- Agregar datos "Afiliación, Empresa Fletera, Tipo de vehículo, Numero economico, Matrículas MX y USA"
	- "Guardar"
		
+ Los Catálogos de Choferes se cargarán en Vehículos y Choferes/:ref:`imut-choferes` 
	- Presionar el botón "Agregar"
	- Capturar datos del chofer "Afiliación, Empresa Fletera, Nombre, Licencias MX y USA"
	- "Guardar"

	
Alta de puntos de Inspección
==============================
+ Los puntos de inspecciones se registran en :ref:`imut-puntos` 
	- Presionar el botón "Agregar" 
	- Capturar detalle del punto de inspección  "Afiliación, Nombre, Texto de ayuda y url de imagen de ayuda"
	- "Guardar"

	
Alta de Inspecciones
==============================
+ Los usuarios web se registrán en :ref:`imut-inspecciones` 
	- Presionar el botón "Agregar", se abrirá la vista "Creación de inspección"
	- Capturar información básica "Nombre de inspección, Descripción, Tipo de Vehículo y Zona"
	- Elegir un logo para este tipo de inspección
	- Seleccionar las configuraciones deseadas
	- Agregar puntos a la plantilla de inspección
	- "Crear Inspección"

Para la configuración de inspecciones, le mostramos una animación con los pasos

.. figure:: imut-web/imut-imspecciones.gif
   :height: 750 px
   :width: 1200 px
   :scale: 60 %
   :alt: alternate text
   :align: center

   Configuración de rondas en TagPatrol

