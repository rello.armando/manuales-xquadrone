====================
Acerca de SGA
====================
.. image:: sga-web/intro-sga.png
   :height: 950 px
   :width: 1600 px
   :scale: 50 %
   :alt: alternate text
   :align: center

====================
SGA Web
====================

.. image:: sga-web/portada.png
   :height: 900 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Login
=====
El usuario ingresará a la aplicación Web, donde el navegante ingresará su Usuario y Contraseña, para después seleccionar el botón de Iniciar para entrar al sistema en caso de que los datos sean correctos.

.. figure:: sga-web/login.png
   :height: 850 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú para inicio de sesión Web


.. note:: Al ingresar información errónea se muestra el siguiente mensaje:

.. image:: sga-web/no-user.png
   :height: 200 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center


.. hint:: En caso de que el usuario haya olvidado su contraseña puede contactar con el equipo de soporte Xquadrone.


Inicio
=======
En la página inicial se muestran las actividades más recientes de la aplicación, los cuales están seccionados por Pases de planta y Personas. Así mismo, las notificaciones de las actividades recientes de Pases e Historial se encuentran en la parte superior derecha de la página, como las opciones de visualización de Información por 24hrs, Semana, Mes, Todo, como se muestra a continuación:

.. figure:: sga-web/dashboard.png
   :height: 850 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Dashboard con el resumen del estado operativo de SGA

Dashboard de Sistema de Gestion de Accesos
==========================================

PASES GENERADOS
----------------
Se muestra la información registrada recientemente indicando dentro del reporte el Número del pase, el Nombre de la empresa, así como la Descripción (Asunto) del pase y la facilidad de la búsqueda rápida y de acomodo por pase, Empresa o Descripción, como se muestra en Dashboard.

PASES DENTRO DE PLANTA
----------------------
Se muestran los pases que se encuentran dentro de la planta indicando el Número de pase, Empresa, Descripción (Asunto) y la facilidad de acomodo por Pase, Empresa o descripción, así como de la búsqueda rápida, como se muestra en Dashboard.

PASES FUERA DE PLANTA
---------------------
Se muestran los pases que salieron de la planta indicando el Número de pase, Empresa, Descripción (Asunto), la facilidad de la búsqueda rápida y del acomodo por Pase, Empresa o Descripción, como se muestra en Dashboard.

INGRESOS DE PLANTA
------------------
Se muestran las personas que entraron a la planta indicando el Número de pase, Empresa, Descripción (Personal) y la facilidad de la búsqueda rápida, así como acomodar por Pase, Empresa o Descripción, como se muestra en Dashboard.

SALIDAS DE PLANTA
-----------------
Se muestra las personas que salieron de la planta indicando el Número de pase, Empresa, Descripción (Personal) la facilidad de la búsqueda rápida, así como el acomodo por Pase o  Descripción, como se muestra en Dashboard.

PERSONAS EN LA PLANTA
----------------------
Se mostrara a todas aquellas personas que esten en la planta actualmente en donde mostrara el nombre de la empresa, el nombre del trabajador, la fecha, la hora, el limite en la planta, el tiempo que ha estado en la planta y permitirá registrar la salida.

ACCESOS EXTRAORDINARIOS
-----------------------
Se muestra información de aquellos accesos extraordinarios que se
generaron recientemente mostrando los campos Pase, Empresa, Descripción (Motivo) así como la facilidad de acomodo por Pase, Descripción y la facilidad de la búsqueda rápida, como se muestra en Dashboard.

DOCUMENTOS POR VENCER
---------------------
Se muestra información de aquellos documentos de trabajadores,
Vehículos y Empresas que se encuentran por vencer recientemente mostrando los campos Nombre, Empresa,
Fecha vencimiento, Obligatorio, así como el acomodo de la información por Nombre, Empresa, Obligatorio
y la facilidad de la búsqueda rápida, como se muestra en Dashboard.

.. hint:: Todas las opciones de actividad reciente cuentan con la opción de búsqueda rápida y exportación
   de información como se muestra en la siguiente imagen

.. figure:: sga-web/busqueda.png
   :height: 240 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center


En el lado superior derecho se tiene visible la opción de “Ver Historial” el cual despliega la siguiente
información que indica la actividad reciente que se está realizando en la aplicación

.. figure:: sga-web/dashboard-historial.png
   :height: 500 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center


En el lado superior izquierdo se encuentra las Características de versión y de Sistema donde se indica los
detalles de la actualización en forma de listado de características.

.. figure:: sga-web/update-detail.png
   :height: 450 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center



Catálogos
=========
Dentro de la sección de catálogos se encuentran los siguientes apartados, Empresas, Trabajadores, Reportes,
Herramientas, Vehículos, Pases, Solicitudes de pases, Estado de pases, Puntos de revisión.

.. _sga-empresa:

Empresas
--------
En esta sección el usuario podrá registrar las empresas que solicitaran pases para poder designarle trabajadores, herramientas, vehículos, para las entradas y salidas.

Se mostrará  la información de las empresas registradas, así como la facilidad de acomodar la información por empresa y la búsqueda rápida mostrando:

- Tipo de empresa
- Responsable
- Correo Electrónico
- Teléfono
- Extensión
- Estatus

Dentro del catálogo se puede verificar todas las empresas y si están en estado Activos o Eliminados.

En la opción de eliminados se mostrarán todas las empresas que se desearon eliminar, y al desplegar la información de cada empresa se tiene la opción de volver activar a la empresa.

Para registrar una empresa

En la parte superior se encuentra la opción de “+Registrar”, el cual una vez que se seleccione hace que se despliegue una ventana solicitando los siguientes datos para que se pueda registrar la empresa correctamente:

- Datos Generales de la empresa
   - Tipo de empresa
   - Nombre
   - RFC
   - Responsable
   - Correo Electrónico de responsable
   - Fecha de inicio
   - Fecha de vencimiento
   - Número de seguro Social
   - Proveedor
   - Teléfono
   - Extensión

- Domicilio
   - Calle
   - Número Exterior
   - Número Interior
   - Colonia
   - CP

Una vez registrado se verá reflejado en el listado de empresas, al desplegar cada una de estas nos encontraremos con las opciones de Editar, Eliminar y Documentos, que al igual que Registro se cargará un diálogo solicitando la información necesaria o confirmación de la acción como se muestra a continuación:



.. figure:: sga-web/listadoDeEmpresas-imagen18.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

- En la opción de Editar desplegará una ventana solicitando los datos necesarios de la empresa el cual puede cambiar.
- En la opción de eliminar solamente se eliminará el registro y la información el registro de la empresa.

En la opción de documentos se desplegará la ventana donde muestra la descripción, la fecha de expedición, campo para agregar documentos, y las opciones de cada empresa como se explicó en el párrafo anterior con las opciones Editar, Eliminar, Ver, como se muestra a continuación:


.. figure:: sga-web/opciondedocumentos-imagen19.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

En la tabla de reporte de la información se puede verificar las siguientes columnas:

- Nombre (Nombre del documento)
- Inicio (Fecha de inicio de estatus)
- Vence (Fecha que vence el documento)
- Estatus:
       - Rojo (Documento Vencido)
       - Anaranjado (Documento por vencer)
       - Verde (Documento en tiempo)


.. figure:: sga-web/TabladeReportedeInfo-Imagen20.png
   :height: 200 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-trabajadores:

Trabajadores
------------
Es todo aquel personal que se registrara dentro de la empresa, permitiendo así generar las entradas y salidas de la empresa.

También se puede realizar búsqueda por empresa y estatus del trabajador indicando la siguiente información:


.. figure:: sga-web/CatalogoTrabajador-Imagen21.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Una vez desplegada la información del trabajador se visualizan las opciones de Perfil, Editar, Eliminar, el cual se describen a continuación:


.. figure:: sga-web/Visualizacion-Imagen22.png
   :height: 500 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

La opción de Perfil indica toda la información requerida para dar de alta un trabajador con los siguientes datos:

- Nombre del trabajador
- Apellido Paterno
- Apellido Materno
- Puesto en la empresa
- Correo electrónico
- Información de domicilio:
   - Calle
   - Número Exterior
   - Numero Interior
   - Colonia
   - Código Postal
   - Foto del trabajador


.. figure:: sga-web/OpciondePerfil-Imagen23.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

La opción de Documentos indica la información requerida para agregar documentos que no son obligatorios al perfil del trabajador, como lo son los siguientes datos:

- Descripción (Nombre del documento)
- Fecha inicial (Vigencia del documento)
- Fecha final (Vencimiento del documento)


.. figure:: sga-web/OpciondeDoc-Imagen24.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center


El apartado para agregar un documento, se encuentra al momento de seleccionar “adjuntar documento”

.. figure:: sga-web/AgregarDoc-Imagen25.png
   :height: 200 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center


El apartado de Documentos Obligatorios indica los documentos que se deben de agregar obligatoriamente para el trabajador configurados con anticipación
 dentro de Configuración/Perfil de documentos, dándole la opción de adjuntar el documento de ser necesario

.. figure:: sga-web/Documentos-Imagen26.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la segunda sección “Usuario con pase” se muestra la siguiente información:

- Nombre (Nombre de la persona que tiene el pase)
- Teléfono
- Correo
- Pase (Descripción de la característica del paseo)


.. figure:: sga-web/Usuariosconpase-Imagen27.png
   :height: 300 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-busqueda:

Búsqueda de Personas
--------------------
En esta sección se podra realizar la búsqueda de cualquier trabajador y así se visualizara al trabajador por medio de filtros, Se filtrara por los siguientes datos:

- Nombre del trabajador
- Apellido Paterno
- Apellido Materno
- NSS
- RFC
- CURP

Se podrá habilitar solo una opción o si, se desea mas de una.

.. _sga-reporte:

Reportes
--------
Dentro de esta sección puede consultar y exportar en Excel la información de los trabajadores por empresa, indicando los siguientes datos:

- Nombre (Trabajador)
- Pase (Tipo de pase)
- Estatus del pase
- Documentos (Estatus de la documentación)
- Fotografía (indica si hay o no fotografía del trabajador)
- Empresa (Nombre)
- Puesto (Puesto que desempeña)
- Correo (Trabajador)
- Teléfono (Donde se puede localizar)
- CURP
- RFC
- NSS
- Estatus del trabajador


.. figure:: sga-web/Reportes-Imagen28.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-herramienta:

Herramientas
------------
Es el equipo de la empresa o igualmente las herramientas traídas por parte de algún trabajador, permitiendo llevar un control de las entradas y salidas de la empresa.

También se podrán consultar y registrar la información de las herramientas por empresa, indicando los siguientes datos en la consulta:

- Nombre
- Serie
- Modelo
- Marca
- Cantidad


.. figure:: sga-web/CatalogodeHerramientas-Imagen29.png
   :height: 500 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para registrar una herramienta nueva selecciona la opción “+Registro” y se desplegara una ventana solicitando la información de la herramienta con los siguientes datos:

- Nombre de la empresa
- Nombre (Herramienta)
- Marca
- Modelo
- Serie
- Cantidad
- Agregar imagen de la herramienta


.. figure:: sga-web/RegistroHeramientas-Imagen30.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center



Para visualizar las opciones de Editar, Eliminar e Imagen de cada Herramienta, en la lista de consultas, cada Herramienta cuenta con un desplegar con una flecha hacia abajo una vez que se selecciona la flecha esta despliega las opciones como se muestra a continuación:


.. figure:: sga-web/RegistroHeramientas-Imagen31.png
   :height: 550 px
   :width: 1100 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar eliminar, se le mostrará el siguiente mensaje y realizará la acción seleccionada.

.. figure:: sga-web/Eliminar-Imagen32.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center


El seleccionar la opción de imagen, esta solo le mostrara una imagen previa de la Herramienta antes registrada:

.. figure:: sga-web/Imagen-Imagen33.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar la opción de Editar Herramienta se despliega una ventana solicitando la información que se puede editar:

- Nombre
- Marca
- Modelo
- Serie
- Cantidad
- Agregar imagen


.. figure:: sga-web/Editar-Imagen34.png
   :height: 750 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-vehiculo:

Vehículos
---------
Son los vehículos de la empresa que podrán tener acceso a las entradas y salidas dentro o fuera.

Se podrá consultar y registrar la información de los vehículos por empresa, indicando los datos de Marca,
Modelo y Placas, así como la búsqueda por empresa y búsqueda rápida como se muestra a continuación:


.. figure:: sga-web/Vehiculos-Imagen35.png
   :height: 200 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para registrar un Vehículo nuevo selecciona la opción “+Registro” y se desplegará una ventana solicitando la información del vehículo con los siguientes datos:

- Marca (Vehículo)
- Modelo (Vehículo)
- Placas (No. Placas)
- Tipo de Vehículo:
   - Vehículo
   - Transporte
   - Contenedor)

- Imagen del vehículo

Como se muestra a continuación:


.. figure:: sga-web/Registro-Imagen36.png
   :height: 600 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para visualizar las opciones de Editar, Eliminar, Ver, Documento de cada Vehículo, en la lista de consultas, cada Vehículo cuenta con un desplegar con una flecha hacia abajo una vez que se selecciona la flecha esta despliega las opciones como se muestra a continuación:


.. figure:: sga-web/Editar-Imagen37.png
   :height: 600 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar eliminar, se le mostrará el siguiente mensaje y realizará la acción seleccionada.


.. figure:: sga-web/Eliminar-Imagen38.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

El seleccionar la opción de ver, esta solo le mostrara una previsualización del Vehículo antes registrado:


.. figure:: sga-web/VehiculoR-Imagen39.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Editar se desplegará una ventana solicitando la información del vehículo con los siguientes datos: Marca (Vehículo), Modelo (Vehículo), Placas (No. Placas), Tipo de Vehículo (Vehículo, Transporte, Contenedor), así como agregar una imagen del vehículo como se muestra a continuación:


.. figure:: sga-web/Editar-Imagen40.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Documento se desplegará una ventana solicitando la información del documento con los siguientes datos: Descripción, Fecha de Expedición, Fecha de vencimiento, así como agregar el documento del vehículo como se muestra a continuación:


.. figure:: sga-web/Doc-Imagen41.png
   :height: 900 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la información desplegada del documento se encuentra la columna "estatus", la cual cuenta con 3 diferentes indicadores Verde, Naranja, Rojo.
	- Verde indica que la fecha de los documentos del vehículo se encuentra a tiempo,
	- Naranja indica que están por vencer los documentos y
	- Rojo indica que se venció el tiempo del pase o documentos de la persona.

Para visualizar las opciones de Editar, Eliminar y ver en cada Documento, la lista de consultas, presione la flecha hacia abajo una vez que se selecciona la flecha, se despliegan las opciones como se muestra a continuación:

.. figure:: sga-web/Desplique-Imagen42.png
   :height: 800 px
   :width: 1300 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Editar se desplegará una ventana solicitando la información del documento con los siguientes datos:

- Descripción
- Fecha de expedición
- Fecha de Vencimiento

Como se muestra a continuación:

.. figure:: sga-web/Imagen43.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar eliminar, se le mostrara el siguiente mensaje y realizara la acción seleccionada.

.. figure:: sga-web/Eliminar-44.png
   :height: 400 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

El seleccionar la opción de ver, se mostrará un mensaje para descargar documento antes registrado:

.. figure:: sga-web/Eliminar-Imagen45.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-pase:

Pases
-----
En esta sección se podra realizar los pases tanto los normales como los extraordinario, lo cual se crearan para poder llevar el control de las entradas y salidas tanto de los empleados, herramientas y vehículo.

Se muestran dos secciones Catálogo de Pases y Pases generados.

+ En Catálogo de pases permite registrar la información de los Pases, indicando los siguientes datos:
	- Tipo de Pase (Normal/Extraordinario)
	- Empresa
	- Asunto
	- Trabajador
	- Herramientas
	- Vehículos
	- Área
	- Fecha inicial (acceso del pase)
	- Fecha Final (Acceso del pase)
	- Numero de pase
	- Numero del área
	- Enviar el pase por correo o imprimir el código QR para su uso

Como se muestra a continuación:

.. figure:: sga-web/Imagen46.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la sección de Pases generados visualiza la información de los Pases, con los datos de Pase:
- Numero de pase
- Empresa
- Asunto
- Fecha de inicio
- Fecha de fin
- Área a visitar
- y Estatus del pase

En esta parte la columna de “Estatus del pase” es el que el administrador asigna, La columna de “Estatus” tiene 3 diferentes estatus (Verde)(Naranja)(Rojo)
- Verde (Correcto) fecha documentos de la persona ok
- Naranja (Advertencia) están por vencer los documentos o el tiempo del pase
- Rojo (Revisar) se venció el tiempo del pase o documentos de la persona si está en tiempo, pero los documentos vencidos se colorean rojo y viceversa como se muestra a continuación:

.. figure:: sga-web/Pases-Imagen47.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para visualizar las opciones de Comentario, Editar, Eliminar, Ver, Estado, Estatus del trabajador de cada pase, en la lista de consultas, cada Pase cuenta con un desplegar con una flecha hacia abajo una vez que se selecciona la flecha esta despliega las opciones como se muestra a continuación:

.. figure:: sga-web/Pases-Imagen48.png
   :height: 500 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Comentario se visualiza una nota que es orientada al pase y si se selecciona "Editar", se despliega una ventana donde solicita la siguiente información que se puede modificar del pase, Asunto, Área, Trabajadores, (Despliega los trabajadores que puedes seleccionar para el pase), Vehículo, Herramientas (Despliega la lista de herramientas que puedes seleccionar para el pase, Fecha inicial, Fecha final como se muestra a continuación:

.. figure:: sga-web/Pases-Imagen49.png
   :height: 400 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar eliminar, se le mostrara el siguiente mensaje y realizara la acción seleccionada.

.. figure:: sga-web/cambioImagen00.png
   :height: 400 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Ver, se le mostrara el pase generado y las opciones, enviar por correo e imprimir.

.. figure:: sga-web/Pases-Imagen51.png
   :height: 800 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Al seleccionar Estatus Trabajador, se puede realizar la búsqueda de los trabajadores por el pase para modificar el estatus en las siguientes opciones: Aceptado, Cancelado, Invalido, Activo, Inactivo.

.. figure:: sga-web/Pases-Imagen54.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Solicitudes de pases
--------------------
En esta sección se puede visualizar un resumen de los pases solicitados por parte de los contratistas, así como la opción de que el administrador pueda asignar un estado al pase que se está solicitando mediante la opción de editar. En la opción de eliminar solo se tiene que confirmar la acción mediante un mensaje que se despliega y el pase solicitado se eliminará de la lista de solicitudes, y la opción ver te indica información del código QR del pase, y en comentarios solo se solicita el comentario que se le quiera agregar a la solicitud.

.. figure:: sga-web/Solicitud-Imagen54.png
   :height: 350 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-estado:

Estados de Pases
----------------
 En esta sección se puede modificar el estatus de los trabajadores, vehículos y herramientas.

Se podrá verificar el estatus del Personal, Vehículo o herramienta.

Sus acciones son:

- Editar
- Buscar
- Filtrar por medio del Pase

Se visualizara los datos como:

- Id Pase
- Pase
- Persona/Vehículo/Herramienta
- Fecha
- Estado
- Empresa


.. _sga-revisión:

Puntos de Revisión
------------------
En esta sección se crear los puntos que se tomaran para realizar la revisión del vehículo.

Dentro del catálogo de Puntos de revisión, se muestra cuatro secciones:

- Registro
- Vehículo
- Transporte
- Contenedor

Indicando que puntos se tienen que revisar en cada tipo de vehículo.

Dentro de la sección de Registro, permite ingresar la información de los puntos de revisión de los vehículos, solicitando el nombre de como se muestra en a continuación:

.. figure:: sga-web/Solicitud-Imagen55.png
   :height: 330 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la sección de Vehículos, permite visualizar la información del punto de revisión del vehículo, eliminar el registro y editar la información del punto de revisión del vehículo, como se muestra en a continuación.

.. figure:: sga-web/Solicitud-Imagen56.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la sección de Transporte, permite visualizar la información del punto de revisión del transporte, eliminar el registro y editar la información del punto de revisión del transporte, como se muestra a continuación.

.. figure:: sga-web/Solicitud-Imagen57.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Dentro de la sección de Contenedor, permite visualizar la información del punto de revisión del Contenedor, eliminar el registro y editar la información del punto de revisión del Contenedor, como se muestra en a continuación.

.. figure:: sga-web/Solicitud-Imagen58.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-bitacoras:

Bitácoras Accesos
=================
Dentro de la sección de Bitácora de Accesos se encuentran los siguientes apartados:

- Bitácora de Pase
- Bitácora Trabajador
- Bitácora Herramienta
- Bitácora Accesos
- Bitácora Vehículo
- Bitácora Extraordinaria

Bitácora de Pase
-----------------
Dentro de esta sección se muestra la información de Pase, Empresa, Fecha, Descripción, Reviso, Extensión, y la vista previa, así como poder acomodar los campos del buscar por medio del filtro en fecha inicial y fecha final de los pases.

.. figure:: sga-web/Bitacora-Imagen59.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Bitácora Trabajador
-------------------
Dentro de esta sección se muestra la siguiente información:

- Pase
- Empresa.
- Fecha
- Descripción
- Reviso
- Extensión
- Previsualización previa
- Búsqueda por medio del filtro por fecha inicial y final de los pases

.. figure:: sga-web/Bitacora-Imagen60.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Bitácora Accesos
----------------
Dentro de esta sección se muestra la información de las entradas, salidas, entradas y salidas, Turnos y el total de entradas y salidas.

- Se filtrara por empresa

Rango de fechas.

- Fecha desde donde desea partir la búsqueda
- Fecha hasta donde desea finalizar la búsqueda

En caso de ser el de turnos.

Se agregara el horario y la hora.

- Matutino
- Vespertino

Bitácora Herramienta
--------------------
Dentro de esta sección se muestra la siguiente información:

- Pase
- Empresa
- Descripción
- Fecha
- Reviso
- Detalles
- Búsqueda por medio del filtro por fecha inicial y final

.. figure:: sga-web/Bitacora-Imagen61.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Bitácora Vehículo
-----------------
Dentro de esta sección se muestra la siguiente información:

- Pase
- Empresa
- Fecha
- Descripción
- Reviso
- Búsqueda por medio del filtro por fecha inicial y final

.. figure:: sga-web/Bitacora-Imagen62.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Bitácora Extraordinaria
-----------------------
Dentro de esta sección se muestra la siguiente información:

- Pase
- Empresa
- Fecha
- Descripción
- Reviso
- Detalles
- Búsqueda por medio del filtro por fecha inicial y final

Configuración
=============

.. _sga-usuario-web:

Usuarios Web
-------------
Son aquellos usuarios que tendrán el acceso al sitio web, cada tipo de usuario tendrá diferentes permisos y solo podrán realizar visualizar lo correspondiente a sus tipo de usuario.

Dentro de esta sección se muestra la siguiente información:

- Pase
- Empresa
- Fecha
- Descripción
- Reviso
- Extensión
- Vista previa

- Búsqueda por medio del filtro por fecha inicial y final de los pases.

.. figure:: sga-web/Usuarios-Imagen64.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

En el área para realizar un registro de Usuario se solicita los datos de Nombre, Correo electrónico, Contraseña, Rol que tendrá el usuario, (Administrador, Contratista, Operador, Operador Contratista, Proveedor, Supervisor) y la empresa a la que pertenece el usuario.

.. figure:: sga-web/Usuarios-Imagen65.png
   :height: 500 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para realizar visualizaciones de los usuarios tienen los filtros de rol y empresa, así como el acomodo de la información en las columnas Nombre, Usuario, y la opción buscar. En la opción editar, se solicita la información que se puede editar.

.. figure:: sga-web/Usuarios-Imagen66.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-usuario-movil:

Usuario Móvil
---------------
 Son los usuarios que van a utilizar la aplicación móvil, los cuales llevaran acaba la revisión de las entradas y salidas de los trabajadores, herramientas y vehículos.

Dentro de esta sección se muestra la siguiente información:

- Nombre
- Usuario
- Contraseña
- Supervisor
- Rol

Acciones

- Registrar
- Editar (solicita la información que se puede actualizar).
- Eliminar (solo se tiene que confirmar que se desea eliminar).
- Búsqueda por medio del filtro por  Nombre, Usuario.

Como se muestra a continuación

.. figure:: sga-web/Usuarios-Imagen67.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Para agregar un rol se tiene que agregar el nombre del rol como los permisos que se le darán acceso, así como para poder editar el rol, en la ventana se solicitan los datos que se pueden editar, y en eliminar solo se tiene que confirmar el mensaje de confirmación.

.. figure:: sga-web/Usuarios-Imagen68.png
   :height: 750 px
   :width: 1000 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-permitir:

Permisos
--------
Son las actividades que se le otorgaran al usuario para poder acceder o realizar solo dichas actividades asignadas.

Editar
^^^^^^^
Se podrá actualizar los permisos de los roles, donde podrá acceder unicamente a los catálogos asignados.

.. figure:: sga-web/Permisos-Editar.png
   :height: 750 px
   :width: 500 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Registro.
^^^^^^^^^

Se podrá registrar un nuevo rol del sistema, otorgando los permisos iniciales, también en este apartado se realizara las acciones como:

- Editar
- Guardar
- Eliminar
- Busqueda por medio del nombre del rol

Como se muestra a continuación.

.. figure:: sga-web/Permisos-Registrar.png
   :height: 750 px
   :width: 500 px
   :scale: 50 %
   :alt: alternate text
   :align: center

Validaciones.
^^^^^^^^^^^^^
Se configuran los permisos que permitirán validar la documentación de los empleados, la empresa, los vehículos y las herramientas.

Acciones
- Habilitar (sera obligatorio contar con los datos)
- Deshabilitar (No es obligatorio que cuente con estos datos)

Validaciones:
- Documentos Obligatorios: Si, se habilita sera obligatorio el uso de documentos del empleado (Se reflejara en el móvil).
- Fotografía de Usuarios: Si, se habilita sera obligatorio la fotografía del empleado (Se reflejara en el móvil).
- Documentos por empresa: Si, se habilita sera obligatorio el uso de documentos en la empresa (Se reflejara en el móvil).
- Estatus por empresa: Si, se habilita sera obligatorio contar con el estatus de la empresa (Se reflejara en el móvil).
- Acceso a Usuarios con Empresa Inválida:Si, se habilita se podrán acceder aunque la empresa no este activa (Se reflejara en el móvil).
- Importancia de Documentos Vehículo:Si, se habilita sera obligatorio el uso de documentos para el vehículoZ (Se reflejara en el móvil).



Como se muestra a continuación.

.. figure:: sga-web/Permisos-Validar.png
   :height: 750 px
   :width: 500 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-documentos:

Perfil Documentos
-----------------
Es una serie de documentos que serán asignados a los trabajadores dependiendo de los roles que posean.

Dentro de esta sección los apartados están divididos en las siguientes áreas:

1. Documentos Obligatorios
2. Acomodar el campo de Nombre
3. Registro de Perfil de Documentos

Acciones

- Eliminar para poder realizar esta acción tiene que confirmar que se desea eliminar o no como se muestra a continuación.

.. figure:: sga-web/Imagen00.png
   :height: 300 px
   :width: 900 px
   :scale: 50 %
   :alt: alternate text
   :align: center

- Búsqueda por medio del filtro por  Nombre.

Para agregar un perfil, en la parte inferior de la sección se solicita el nombre de perfil, seleccionar los documentos obligatorios para el perfil creado, así como en la información que se muestra es el nombre, las opciones de editar el cual solicita los datos que se pueden actualizar del perfil y eliminar solo se tiene que confirmar que se desea eliminar como se muestra anteriormente.

.. _sga-asignacion:

Asignación de Perfiles
----------------------
Dentro de esta sección se realizan las asignaciones de perfil por trabajador de cierta empresa:

Para agregar un perfil de trabajador masivo.

1. Se solicita el nombre de la empresa, él o los trabajadores que se desean dar de alta.
2. Seleccionar el perfil que se les desea asignar.

La información que se muestra es el nombre del trabajador, Empresa y el perfil antes asignado. La información se puede reacomodar por Nombre y por Empresa.

.. figure:: sga-web/asignacionPerfiles-Imagen71.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center


Acciones

- Guardar
- Búsqueda por medio del filtro por  Nombre

.. _sga-correos:

Correos de Alertas
------------------
Dentro de esta sección se realizan el registro de los correos y la asignación de los tipos de alertas que recibirá cara correo:

Para agregar un correo:

1. Se solicita el correo electrónico, el tipo de alerta el que se desea recibir.
2. Guardar.
3. La información que se muestra es el Correo electrónico, Alertas.

Acciones

- Editar.
- Eliminar.
- Acomodar la información por Correo electrónico y por Alerta.
- Búsqueda por medio del filtro por  Correo electrónico, Alertas.

.. figure:: sga-web/CorreosDeAlertas-Imagen72.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _sga-carga:

Cargar CSV
----------
Para la carga masiva de Empresas (solamente los usuarios de tipo administrador pueden realizar el alta), Trabajadores, Vehículos o Herramientas, solamente es necesario llenar los archivos de que se envían a los administradores o contratistas, una vez se llenan los campos que se solicitan se convierte el archivo de Excel a CSV y se cargan en su respectivo campo. Si se presenta un problema o registro duplicado se mostrará en el listado indicando el motivo por el cual no se registró.

.. _sga-ajustes:

Ajustes
-------
Dentro de esta sección se realizan los ajustes de las empresas con el tipo de empresa, así como el límite de tiempo que tendrá cada tipo de empresa

.. _sga-registrodoc:

Documentos
----------
Dentro de esta sección realizan los registros de los tipos de formatos permitidos de los documentos, una vez que se registra el formato, se refleja la información Extensión, Estatus, la opción:

- Editar.
- Eliminar.
- Búsqueda por medio del filtro por Extensión.

.. figure:: sga-web/documentos-Imagen75.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

.. _dispositivo-movil:

Dispositivos móviles
--------------------
Se refiere a aquellos dispositivos los cuales estarán asignados a los usuarios móviles, lo cual permitirá instalar la aplicación móvil en el.

Dentro de esta sección realizan los registros del modelo y del IMEI de los teléfonos que se utilizaran dentro de la aplicación móvil, una vez que se registra el modelo y el IMEI, se refleja la información Modelo, IMEI, el estatus, las opciones de editar y Eliminar.

.. figure:: sga-web/documentos-Imagen76.png
   :height: 650 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center





Ayuda adicional
===============
• Permisos por rol Administrador: Tiene derecho a todas las funcionalidades del sistema.
• Catálogos: Pases, Empresas, Personas, Herramientas, Vehículos, Usuarios, Bitácoras, Móvil, Correos, Perfil de trabajador.
• Puntos de revisión, Ajustes.
• Crear, Modificar, Leer, Eliminar.
• Contratista: Tiene derecho solamente lo necesario para crear un pase e ingresar la información de los trabajadores.
• Catálogos: Pases, Personas, Herramientas, Vehículos, Perfil de trabajador.
• Crear, Modificar, Leer, Eliminar.
• Catálogo de empresa.
• Crear, Modificar, Leer.
• Visualizar: Tiene derecho al catálogo de pases.
• Catálogo de pases
• Lectura



Preguntas frecuentes:
=====================
¿Se puede asignar personal a un pase sin tener documentos ingresados?
   R: No, no se puede tener acceso a un pase cuando no se tienen los documentos obligatorios y la vigencia correctamente.
¿Un contratista puede dar de alta su propia empresa?
   R: Solamente el rol de administrador puede dar de alta y asigna la empresa a los contratistas.
¿Cómo puedo darle acceso a una persona que urge que entre a la planta y que no está asignado a un pase?
   R: Existe la opción de pase extraordinario donde se puede asignar un pase a una persona sin necesidad de ingresar documentos obligatorios.
¿Puedo asignar un pase a varias personas?
   R: Se puede crear un pase y asignarle varias personas al mismo pase para facilidad de acceso.
¿Puedo dar de alta pases sin tener documentos en la empresa?
   R: No, Es necesario tener mínimo 1 documento para que se habilite el apartado de generar pase.
¿Por qué no veo a todos los trabajadores de la empresa para asignarlos a un pase?
   R: Los trabajadores que aparecen en la lista de trabajadores cuando creas un pase son los trabajadores que cumplen con los documentos en regla, así como la vigencia vigente, el personal que no aparezca dentro de la lista es porque le falta documentación obligatoria o no se encuentra vigente su acceso.
¿Puedo agregar varios trabajadores al sistema al mismo tiempo?
   R: Si, Apartado; Configuración/Cargar CSV. Se agregan vistas de formatos para agregar información masiva.
Empresas
  Herramientas
Trabajadores
  Vehículos



Si desea obtener apoyo técnico o ayuda en relación con el software, póngase en contacto con el equipo de Xquadrone https://www.xquadrone.mx/, tel. 8007881762, 6865645871.
