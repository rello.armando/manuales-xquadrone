﻿===================
IMUT Móvil
===================


Iniciar sesión / Cerrar sesión 
===============================

Iniciar sesión
--------------

Para iniciar sesión deberemos ingresar el número de empleado asignado y hacer clic en el botón INICIAR

En el campo usuario se deberá escribir el nombre de usuario.

La función de este botón es validar el número de usuario para dar acceso a las funcionalidades de la aplicación.



.. figure:: imut-movil/login.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Login Móvil de IMUT

Cerrar sesión
-------------

Para cerrar sesión tenemos que hacer clic en el menú y seleccionar la opción de “Cerrar Sesión” que se encuentra en el submenú de configuración.

La función de este botón es cerrar la sesión del usuario.


.. figure:: imut-movil/menu.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Cerrar sesión Móvil de IMUT

Inspecciones
=============

Listado de inspecciones
-----------------------

En esta opción se muestran todas las inspecciones, la información a mostrar es la siguiente:
              ● Nombre Ej. Inspección para vehículos tipo van

              ● Breve descripción

              ● Imagen de portada como ayuda visual.

.. figure:: imut-movil/inspecciones.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Inspecciones Móvil de IMUT

Capturar Inspección
-------------------

A) Datos de la inspección

   En esta sección se deberán capturar todos los puntos de inspección.
   Se deberá ingresar los datos generales de la unidad inspeccionada (vehículo, chofer, caja, tipo de carga).

La siguiente imagen muestra los elementos principales de esta sección:

.. figure:: imut-movil/vehiculo.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Registro de vehículos.

.. figure:: imut-movil/chofer.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Registro de chofer.

.. figure:: imut-movil/caja.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Registro de Caja.



Puntos de inspección
====================
Se muestra el listado de puntos de inspección a revisar, con sus opciones de respuesta. También se encuentra una imagen como ayuda visual por punto de inspección.

A) Ayuda visual

    1. Seleccionar para visualizar la imagen de guía del punto de inspección.
       La imagen mostrada será referente al punto de inspección a revisar como una guía visual para su fácil 
       entendimiento.

.. figure:: imut-movil/ayuda.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Fig 7 - Pantalla muestra información del punto a revisar.

.. note:: Esta imagen se registra por medio de la administración web cuando se configura un punto de inspección.

B) Opción de respuesta
  
    1. Seleccionar una opción de respuesta

    2. Si la opción seleccionada genera evidencia se mostrará un campo de texto para agregar una observación y la 
    opción de agregar una imagen.
  
    
    3. La opción Terminar guarda las respuestas seleccionadas con las imágenes adjuntas si es el caso y avanza al
    siguiente paso (Firmar inspección y enviar).


.. figure:: imut-movil/puntoRevision.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Fig 8 - Pantalla muestra los Punto de Inspección a Revisar.


.. note:: Si la opción que genera evidencia está configurada para terminar con la inspección una vez seleccionada esta opción, todos los puntos de inspección serán bloqueados excluyendo los que ya fueron contestados dejando como una opción el terminar la inspección.


Resumen de Inspección y firmas
===============================

Se muestra el número de puntos de inspección contestados de las diferentes opciones seleccionadas y se agrega el estatus de la inspección, así como las firmas de las personas involucradas en la revisión (revisor, embarques y chofer de la unidad).

A) Estatus de la inspección

   1. Seleccionar un estatus para la inspección (Autorizado, Rechazado); si selecciona la opción de Rechazado se activará una caja de texto para ingresar el motivo del rechazo.

B) Firmas

   1. Agregar las firmas de las personas involucradas en la inspección (Revisor, Embarques, chofer).
   2. Seleccionar de la lista de Embarques a la persona que firmará.

C) Aceptar Respuestas Inspección

   1. La función de este botón es enviar la inspección con toda la información capturada

.. figure:: imut-movil/firma.png
   :height: 900 px
   :width: 450 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   
   Fig 9 - Pantalla muestra las firmas del Punto de Inspección.
