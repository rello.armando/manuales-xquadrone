====================
SGA Web Contratistas
====================

El usuario ingresará a la aplicación Web, donde el navegante ingresará su Usuario y Contraseña, para después seleccionar el botón de Iniciar para entrar al sistema en caso de que los datos sean correctos.

.. figure:: sga-web/login.png
   :height: 800 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú para inicio de sesión Web


.. note:: Al ingresar información errónea se muestra el siguiente mensaje:

.. image:: sga-web/no-user.png
   :height: 200 px
   :width: 600 px
   :scale: 50 %
   :alt: alternate text
   :align: center


.. hint:: En caso de que el usuario haya olvidado su contraseña puede contactar con el Administrador del sistema.


Inicio
=======
En la página inicial se muestran las actividades más recientes y el estatus de los pases, 

.. figure:: sga-web/contratista-dashboard.png
   :height: 700 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Dashboard con el resumen del estado operativo de SGA
   
   
Dashboard de Sistema de Gestión de Accesos
==========================================
- En el dashboard podrá observar las siguientes tarjetas con la información de los Pases:
   - Generados
   - Aceptados
   - Cancelados
   - Y Documentos por vencer  



Menú Catálogos
==============
En este menú encontrara las siguientes opciones:
   - Trabajadores
   - Reportes
   - Herramientas
   - Vehículos


.. figure:: sga-web/contratista-menu-catalogos.png
   :height: 700 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú Catálogos
   
   
Trabajadores
------------
En esta sección aparece un listado de los trabajadores que tenga capturados en su empresa y los que cuentan con pase. En la tabla aparecen los datos básicos así como
el estatus del empleado y si cuenta con fotografía registrada.

.. figure:: sga-web/contratista-trabajadores.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú Trabajadores

Registro de empleados
------------------------   
Para registrar a los empleados de su empresa deberá presionar el botón **Registrar** **(1)** saldrá una forma donde se llenan los datos 
básicos del empleado, además deberá seleccionar el "perfil del trabajador" ya que de esto dependerá qué documentos se le solicitarán cargar en el sistema.

.. figure:: sga-web/contratista-trabajadores-registro.png
   :height: 800 px
   :width: 500 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Registro de Trabajadores
   
Alta de documentos del trabajador
---------------------------------
Para dar de alta los documentos por trabajador, pulse el botón  **(1)** y luego el botón perfil **(2)** para cargar o modificar los documentos básicos 
y obligatorios **(3)** en caso de que el perfíl de trabajador lo requiera

.. figure:: sga-web/contratista-trabajadores-alta.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Registro de Trabajadores
	

Reportes
--------
Registro de trabajadores dados de alta en la empresa en una tabla exportable a Excel.

Herramientas
------------
Registro de herramientas registradas por la empresa para agregar a los pases de entrada 

Vehículos
---------
Registro de vehículos de la empresa para solicitar acceso en los pases

Pases
-----   
Menú para capturar las solicitudes de pase donde deberá llenar la siguiente información:

.. figure:: sga-web/contratista-pases.png
   :height: 600 px
   :width: 1200 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú para capturar solicitudes de pase
 
 
1. **ASUNTO:** Descripción del motivo por el cual se solicita el pase.
2. **TRABAJADOR:** Catalogo de trabajadores dados de alta en la empresa y que cuentan con todos sus documentos vigentes. Si le falta algún documento o no esta vigente, no aparecerá.
3. **HERRAMIENTA:** Catalogo para seleccionar herramienta previamente dada de alta. (opcional)
4. **VEHICULO:** Catalogo de vehículos capturados en la empresa. Es necesario que estén vigentes sus documentos en el sistema para que este habilitado para selección. (opcional)
5. **AREA:** Área dentro de la empresa a la que se solicita el pase.
6. **FECHA INICIAL:** Inicio de la vigencia solicitada.
7. **FECHA FINAL:** Termino de la vigencia del pase.

   
* Es posible actualizar algún dato del pase solicitando que se autorice nuevamente.*
   
Una vez que se han capturado todos los campos obligatorios se puede generar el pase al presionar **Generar Pase**, esto manda automáticamente un correo electrónico
a la persona que se registro como responsable de la empresa. 

Configuración
=============

.. figure:: sga-web/contratista-menu-configuracion.png
   :height: 800 px
   :width: 400 px
   :scale: 50 %
   :alt: alternate text
   :align: center

   Menú Configuración

Contratista
-----------
En esta sección se se pueden consultar los datos que se de alta de la empresa

Carga CSV
---------
Esta sección es para utilizar la funcionalidad de carga masiva de empleados, herramientas y vehículos. Para ello podrá descargar de ahí mismo plantillas en excel que se pueden llenar e importar al sistema.



Ayuda adicional
===============
• Permisos del Contratista: Tiene derecho a cargar y modificar información de los documentos de su empresa, empleados, herramientas y vehículos, así como lo necesario para crear un pase.


Preguntas frecuentes:
=====================
¿Se puede asignar personal a un pase sin tener documentos ingresados?
   R: No, no se puede tener acceso a un pase cuando no se tienen los documentos obligatorios y la vigencia correctamente.
¿Un contratista puede dar de alta su propia empresa?
   R: Solamente el rol de administrador puede dar de alta y asigna la empresa a los contratistas.
¿Cómo puedo darle acceso a una persona que urge que entre a la planta y que no está asignado a un pase?
   R: Existe la opción de pase extraordinario donde se puede asignar un pase a una persona sin necesidad de ingresar documentos obligatorios.
¿Puedo asignar un pase a varias personas?
   R: Se puede crear un pase y asignarle varias personas al mismo pase para facilidad de acceso.
¿Puedo dar de alta pases sin tener documentos en la empresa?
   R: No, Es necesario tener mínimo 1 documento para que se habilite el apartado de generar pase.
¿Por qué no veo a todos los trabajadores de la empresa para asignarlos a un pase?
   R: Los trabajadores que aparecen en la lista de trabajadores cuando creas un pase son los trabajadores que cumplen con los documentos en regla, así como la vigencia vigente, el personal que no aparezca dentro de la lista es porque le falta documentación obligatoria o no se encuentra vigente su acceso.
¿Puedo agregar varios trabajadores al sistema al mismo tiempo?
   R: Si, Apartado; Configuración/Cargar CSV. Se agregan vistas de formatos para agregar información masiva.


Si desea obtener apoyo técnico o ayuda en relación con el software, póngase en contacto con el equipo de Xquadrone
 https://www.xquadrone.mx/, tel. 8007881762, 6865645871.
